﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Haszowanie
{
    class HashList
    {
        private uint size;
        private int count = 0;

        public double Count
        { get { return count; } }

        private List<string>[] list;

        //delegat funkcji haszujacej
        public delegate uint dHash(string val, uint size);
        public dHash Hash;


        #region KONSTRUKTOR
        public HashList(uint size)
        {
            this.size = size;

            Hash = HashFun.Default;

            list = new List<string>[size];

            for (int i = 0; i < size; i++)
                list[i] = new List<string>();
        }

        public HashList(uint size, dHash HashFun)
        {
            this.size = size;

            Hash = HashFun;

            list = new List<string>[size];

            for (int i = 0; i < size; i++)
                list[i] = new List<string>();
        }
        #endregion

       
        private void ResizeList(uint new_size)
        {
            List<string> TMP_list = new List<string>();


            //kopiowanie wszystkich wartosci do tymczasowej list
            foreach (var l in list)
                foreach (var t in l)
                    TMP_list.Add(t);

            //powiekszanie tablicy
            size = new_size;

            list = new List<string>[size];

            for (int i = 0; i < size; i++)
                list[i] = new List<string>();

            count = 0;

            foreach (var l in TMP_list)
                Add(l);

            TMP_list.Clear();

        }
    

        #region funkcja dodajaca wartosc do listy, zwraca index
        public int Add(string val)
        {
            int idx;

            idx = (int)Hash(val, size);


            if (!Mem(val,idx)){
                list[idx].Add(val);
                count++;
            }


            if (count >= size)
                ResizeList( 2 * size);

            return idx;
        }

        public int Del(string val)
        {
            int idx;

            idx = (int)Hash(val, size);

            int id = list[idx].IndexOf(val);

            //usuwanie wartosci, gdy znajduje sie w liscie
            if (id >= 0){
                list[idx].RemoveAt(id);
                count--;
            }

            if (count <= (size/2))
                ResizeList((uint)(size/2));

            return idx;
        }

        public bool Mem(string val)
        {
            int idx = (int)Hash(val, size);

            return list[idx].Contains(val);
        }

        public bool Mem(string val, int idx)
        {
            return list[idx].Contains(val);
        }
        #endregion

        #region SHOW
        public string ShowIdx(int idx)
        {
            if (idx >= size)
                throw new IndexOutOfRangeException("Wartosc poza lista");

            var str = String.Join(", ", list[idx].ToArray());

            return str;
        }



        public void Show()
        {
            Show(0, (int)(size - 1));
        }

        public void Show(int to)
        {
            Show(0, to);
        }

        public void Show(int from, int to)
        {
            if (from < 0 || from >= size || from > to || to >= size)
                throw new IndexOutOfRangeException();

            for (int i = from; i <= to; i++)
            {
                
                if (list[i].Count != 0)
                {
                    Console.WriteLine("{0} ({1}).\t{2}", i + 1, list[i].Count, ShowIdx(i));
                }
                else
                    WriteGreen(((i + 1) + "."));
            }

        }

        public void Stat()
        {
            int[] cop = new int[count + 1];

            for (int i = 0; i < count + 1; i++)
                cop[i] = 0;

            foreach(var l in list)
            {
                cop[l.Count] += 1;
            }

            Console.WriteLine("*--------------------------------------------------*");
            Console.WriteLine("Liczba elementów: {0}\nWielkosc tablicy: {1}", count, size);

            Console.WriteLine("liczba elementów na pozycje");
            for (int i = 0; i < 10; i++)
                if(cop[i] > 0) Console.WriteLine(i + "   " + cop[i]);

            Console.WriteLine("*--------------------------------------------------*");

        }


        //misc
        static void WriteGreen(string value)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(value);

            Console.ResetColor();
        }
        #endregion


        public static class HashFun
        {
            public static uint Default(string val, uint size)
            {
                int i;
                uint h = 0;

                for (i = 0; i < val.Length; i++)
                    h += (uint)val[i];

                return (uint)h % size;
            }

            public static uint Djb2(string val, uint size)
            {
                int i;

                ulong h = 5381;

                for (i = 0; i < val.Length; i++)
                {
                    var c = val[i];
                    h = ((h << 5) + h) + c;
                }

                return (uint)h % size;
            }

            public static uint Sdbm(string val, uint size)
            {
                int i;

                ulong h = 0;

                h = 0;

                for (i = 0; i < val.Length; i++)
                {
                    var c = val[i];
                    h = c + (h << 6) + (h << 16) - h;
                }

                return (uint)h % size;
            }

            public static uint Multi31(string val, uint size)
            {
                uint h = 7;
                for (int i = 0; i < val.Length; i++)
                {
                    var c = val[i];
                    h = h * 31 + c;
                }

                return h % size;
            }

            public static uint Sfold(string s, uint size)
            {
                int intLength = s.Length / 4;
                long sum = 0;

                for (int j = 0; j < intLength; j++)
                {
                    char[] ch = (s.Substring(j * 4, (j * 4) + 4)).ToCharArray();
                    long mul = 1;
                    for (int k = 0; k < ch.Length; k++)
                    {
                        sum += ch[k] * mul;
                        mul *= 256;
                    }
                }

                char[] c = s.Substring(intLength * 4).ToCharArray();
                long mult = 1;
                for (int k = 0; k < c.Length; k++)
                {
                    sum += c[k] * mult;
                    mult *= 256;
                }

                return (uint)(sum % size);
            }

        }

    }
}
