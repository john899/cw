﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzeplywyProste
{
    class Program
    {
        static public int[][] graph5()
        {
            int nodes = 6;

            int[][] graph = new int[nodes][];
            for (int i = 0; i < nodes; i++)
            {
                graph[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 16;
            graph[0][2] = 13;

            graph[1][2] = 10;
            graph[1][3] = 12;

            graph[2][1] = 4;
            graph[2][4] = 14;

            graph[3][2] = 9;
            graph[3][5] = 20;

            graph[4][3] = 7;
            graph[4][5] = 4;

            return graph;
        }

        static public int[][] graph1()
        {
            int nodes = 4;

            int[][] graph = new int[nodes][];
            for (int i = 0; i < nodes; i++)
            {
                graph[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 1;
            graph[0][2] = 2;

            graph[1][3] = 3;

            graph[2][3] = 4;

            return graph;
        }

        static public int[][] graph2()
        {
            int nodes = 4;

            int[][] graph = new int[nodes][];
            for (int i = 0; i < nodes; i++)
            {
                graph[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 1;
            graph[0][2] = 2;

            graph[1][3] = 3;

            graph[2][3] = 4;
            graph[2][1] = 1;

            return graph;
        }

        static public int[][] graph3()
        {
            int nodes = 4;

            int[][] graph = new int[nodes][];
            for (int i = 0; i < nodes; i++)
            {
                graph[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 4;
            graph[0][2] = 8;

            graph[1][3] = 2;

            graph[2][3] = 3;
            graph[2][1] = 7;

            return graph;
        }


        static void Main(string[] args)
        {
            int nodes = 4;

            int[][] graph = graph1();

            Flow.Run(graph, nodes);

            Console.ReadKey();
        }
    }
}
