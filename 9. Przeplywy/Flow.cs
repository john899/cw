﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzeplywyProste
{
    static class Flow
    {
        static private int[][] flow;
        static private int[][] resid;
        static private int[][] path;

        static public void Run(int[][] graph, int nodes)
        {     
            path = InitGraph(nodes);
            flow = InitGraph(nodes);

            resid = graph;        

            while (path != null)
            {
                resid = Sub(resid, path);

                Console.WriteLine("\ngraf rezydualny: ");
                ShowGraph(resid);

                flow = Add(flow, path);

                Console.WriteLine("\naktualny graf przeplywu: ");
                ShowGraph(flow);

                path = DFS.GetPath(resid, nodes);

                Console.WriteLine("\nsciezka powiekszajaca: ");
                ShowGraph(path);

                Compress(ref path);

            }

            Console.WriteLine("\n\n\nKoniec algorytmu");
            ShowGraph(flow);
        }



        static private int[][] Add(int[][] A, int[][] B)
        {
            int n = A.Count<int[]>();
            int[][] mat = new int[n][];

            for (int i = 0; i < n; i++)
                mat[i] = new int[n];

            for(int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    mat[i][j] = A[i][j] + B[i][j];
            }

            return mat;
        }
  
        static private int[][] Sub(int[][] A, int[][] B)
        {
            int n = A.Count<int[]>();
            int[][] mat = new int[n][];

            for (int i = 0; i < n; i++)
                mat[i] = new int[n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    mat[i][j] = A[i][j] - B[i][j];
            }

            return mat;
        }



        static private int MinEdgeVal(int[][] path)
        {
            int min = int.MaxValue;
            int n = path.Count<int[]>();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if ((min > path[i][j]) && path[i][j] != 0)
                        min = path[i][j];
                }
            }

            if (min == int.MaxValue)
                throw new Exception();

            return min;

        }

        static private void Compress(ref int[][] A)
        {
            if (A == null)
                return;

            int n = A.Count<int[]>();
            int min = MinEdgeVal(A);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    if (A[i][j] != 0)
                        A[i][j] = min;
            }

        }



        static private void ShowGraph(int[][] graph)
        {
            if (graph == null)
                return;

            int n = graph.Count<int[]>();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    if (graph[i][j] != 0)
                        Console.WriteLine(i + " --(" + graph[i][j] + ")--> " + j);                   
            }

        }

        static private int[][] InitGraph(int size)
        {
            int[][] graph = new int[size][];

            for (int i = 0; i < size; i++)
            {
                graph[i] = new int[size];
                for (int j = 0; j < size; j++)
                    graph[i][j] = 0;
            }

            return graph;
        }



    }
}
