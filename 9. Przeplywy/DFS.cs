﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzeplywyProste
{
    static class DFS
    {
        static int n;                 
        static int[][] graph;          
        static bool[] visited;          
        static int[][] path;           
        static bool found;

        static void DFSRec(int v)
        {
            visited[v] = true;
            //Console.Write(v + " ");

            for (int i = 0; i < n; i++)
            {
                if ((graph[v][i] > 0) && !visited[i] && !found)
                {
                    if (i == (n - 1))
                        found = true;

                    path[v][i] = graph[v][i]; //dodawanie krawedzi do sciezki

                    DFSRec(i); //przechodzenie dalej rekurencyjnie
                }
                
                if (!found)
                    path[v][i] = 0; //usuwanie przy cofaniu

            }

        }

        static public int[][] GetPath(int[][] graph, int nodes)
        {
            n = nodes;
            DFS.graph = graph;

            found = false;

            path = new int[nodes][];
            for (int i = 0; i < n; i++)
            {
                path[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    path[i][j] = 0;
            }

            visited = new bool[nodes];

            for (int i = 0; i < nodes; i++)
                visited[i] = false;

            DFSRec(0);

            if (found)
                return path;
            else
                return null;
        }


    }
}
