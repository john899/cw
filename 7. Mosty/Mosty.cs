﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mosty
{
    class Mosty
    {
        static int n;
        static int[][] graph;

        static Node[] node;
        static bool[] visited;

        static List<int> bridge;
        static List<int> articulation;

        static private int d_iterator;

        static bool debug = false;

        static void DFSRec(int v)
        {
            visited[v] = true;
            if(debug) Console.Write(v + " ");

            node[v].d = d_iterator++;

            for (int i = v; i < n; i++)
            {
                if (graph[v][i] > 0)
                {
                    node[v].kw.Add(i); //dodawanie wszystkich krawedzi wtornych
                    node[i].kw.Add(v); //dodawanie wszystkich krawedzi wtornych
                }
            }


            for (int i = v; i < n; i++)
            {
                if ((graph[v][i] > 0) && !visited[i])
                {
                    node[i].parent = v;

                    node[v].child.Add(i);
                    node[v].kw.RemoveAt(node[v].kw.IndexOf(i));
                    node[i].kw.RemoveAt(node[i].kw.IndexOf(v));

                    DFSRec(i);
                }
            }

            //liczenie funkcji low w drodze powrotnej
            node[v].low = Low(v); 


            //okreslanie czy most
            if ((node[v].low == node[v].d) && node[v].parent != -1) bridge.Add(v);

            //znajdowanie punktow artykulacji
            if (node[v].parent != -1)
            {
                foreach (var c in node[v].child)
                {
                    if (node[c].low >= node[v].d)
                    {
                        articulation.Add(v);
                        break;
                    }

                } 
            }
            else
            {
                if (node[v].child.Count > 1)
                    articulation.Add(v);
            } 


        }


        static private int Low(int idx)
        {
            int min_l_c = int.MaxValue;
            int min_d_kw = int.MaxValue;

            foreach (var c in node[idx].child)
                if (node[c].low < min_l_c)
                    min_l_c = node[c].low;

            foreach (var kw in node[idx].kw)
                if (node[kw].d < min_d_kw)
                    min_d_kw = node[kw].d;

            return Math.Min(Math.Min(min_l_c, node[idx].d), min_d_kw);
        }

        static private void Init(int[][] in_graph)
        {
            d_iterator = 1;

            graph = in_graph;
            n = graph.Count<int[]>();

            bridge = new List<int>();
            articulation = new List<int>();

            node = new Node[n];

            visited = new bool[n];

            for (int i = 0; i < n; i++)
            {
                node[i] = new Node();
                visited[i] = false;
            }

        }


        static public void Run(int[][] in_graph)
        {
            Init(in_graph);

            DFSRec(0);
            Console.WriteLine();


            if (debug)
            {
                int i = 0;
                foreach (var n in node) Console.WriteLine((i++) + " rodzic= " + n.parent);

                i = 0;
                Console.WriteLine("DZIECI: ");
                foreach (var n in node)
                {
                    Console.WriteLine("dzieci: " + (i++));
                    foreach (var c in n.child)
                        Console.Write(c + " ");
                    Console.WriteLine();
                }

                Console.WriteLine("CZAS: ");
                i = 0;
                foreach (var n in node) Console.WriteLine((i++) + " czas= " + n.d);

                i = 0;
                Console.WriteLine("KW: ");
                foreach (var n in node)
                {
                    Console.WriteLine("kw: " + (i++));
                    foreach (var c in n.kw)
                        Console.Write(c + " ");
                    Console.WriteLine();
                }

                Console.WriteLine("LOW: ");
                i = 0;
                foreach (var n in node) Console.WriteLine((i++) + " low= " + n.low);
            }

            Console.WriteLine("\nMOSTY:");
            foreach (var b in bridge)
                Console.WriteLine("Most: " + node[b].parent + " -- " + b);


            Console.WriteLine("\nPUNKTY ARTYKULACJI:");
            foreach (var a in articulation)
                Console.WriteLine("Punkt: " + a);

        }

    }

    class Node
    {
        public int low;
        public int parent;
        public int d;
        public List<int> kw;
        public List<int> child;

        public Node()
        {
            low = int.MaxValue;
            parent = -1;
            d = -1;

            kw = new List<int>();
            child = new List<int>();
        }
    }
}
