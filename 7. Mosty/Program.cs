﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mosty
{
    class Program
    {

        static public int[][] GraphL3()
        {
            int nodes = 3;

            int[][] graph = new int[nodes][];
            for (int i = 0; i < nodes; i++)
            {
                graph[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 1;
            graph[1][2] = 1;

            return graph;
        }

        static public int[][] GraphT3()
        {
            int nodes = 3;

            int[][] graph = new int[nodes][];
            for (int i = 0; i < nodes; i++)
            {
                graph[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 1;
            graph[0][2] = 1;
            graph[1][2] = 1;


            return graph;
        }

        static public int[][] Graph3()
        {
            int nodes = 7;

            int[][] graph = new int[nodes][];
            for (int i = 0; i < nodes; i++)
            {
                graph[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 1;
            graph[0][2] = 1;

            graph[1][2] = 1;

            graph[2][3] = 1;

            graph[3][4] = 1;
            graph[3][6] = 1;

            graph[3][4] = 1;
            graph[3][6] = 1;

            graph[3][6] = 1;

            graph[4][5] = 1;

            graph[5][6] = 1;

            return graph;
        }

        static public int[][] Graph4()
        {
            int nodes = 7;

            int[][] graph = new int[nodes][];
            for (int i = 0; i < nodes; i++)
            {
                graph[i] = new int[nodes];
                for (int j = 0; j < nodes; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 1;
            graph[0][4] = 1;

            graph[1][2] = 1;
            graph[1][3] = 1;

            graph[2][3] = 1;

            graph[4][5] = 1;
            graph[4][6] = 1;

            return graph;
        }

        static void Main(string[] args)
        {
            int[][] graph = Graph4();

            Console.WriteLine("GRAF: ");
            ShowGraph(graph);

            Mosty.Run(graph);

            Console.ReadKey();

        }


        static void ShowGraph(int[][] graph)
        {
            int n = graph.Count<int[]>();

            for(int i = 0; i < n; i++)
            {
                for(int j = i; j < n; j++)
                {
                    if (graph[i][j] > 0)
                    Console.WriteLine(i + " -- " + j);
                }
            }


        }
    }
   
}
