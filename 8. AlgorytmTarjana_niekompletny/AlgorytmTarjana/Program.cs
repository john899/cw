﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmTarjana
{
    class Program
    {
        static void Main(string[] args)
        {
            Tarjan t = new Tarjan();

            t.Run();

            Console.ReadKey();

        }
    }
}
