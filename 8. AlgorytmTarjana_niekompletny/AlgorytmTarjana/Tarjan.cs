﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmTarjana
{
    class Vert
    {
        public string name;
        public int rank;
        public string colour = "white";

        public Vert parent;
        public Vert ancestor;
        public List<Vert> children = new List<Vert>();

        public Vert(string name)
        {
            this.name = name;

            parent = this;
            rank = 0;
        }

    }

    class Tarjan
    {
        Vert[] T;

        public Tarjan()
        {
            T = new Vert[8];

            T[0] = new Vert("a"); //root

            T[1] = new Vert("b");
            T[2] = new Vert("g");

            T[3] = new Vert("h");
            T[4] = new Vert("i");
            T[5] = new Vert("c");
            T[6] = new Vert("d");

            T[7] = new Vert("j");

            /*
            T[0].parent = T[0]; //root

            T[1].parent = T[0];
            T[2].parent = T[0];
            T[3].parent = T[2];
            T[4].parent = T[2];
            T[5].parent = T[1];
            T[6].parent = T[1];
            T[7].parent = T[3];
            */

            for (int i = 1; i < 7; i++)
                T[0].children.Add(T[i]);

            T[3].children.Add(T[7]);


            T[1].children.Add(T[5]);
            T[1].children.Add(T[6]);


            T[2].children.Add(T[3]);
            T[2].children.Add(T[4]);
            T[2].children.Add(T[7]);


        }

        public void Run()
        {
            Init();
            Console.WriteLine(Find(T[6]).name);

            TarjanREC(T[0]);

        }

        void Init(){
            for(int i = 0; i< T.Count<Vert>(); i++)
                T[i].colour = "white";
        }

        void MakeSet(Vert x){
               x.parent = x;
               x.rank = 0;
        }



        public void TarjanREC(Vert u)
        {
            MakeSet(u);
            u.ancestor = u;

            for (int i = 0; i < u.children.Count; i++)
            {
                Vert v = u.children[i];

                TarjanREC(v);
                Union(u, v);
                Find(u).ancestor = u;
            }
             
            u.colour = "black";

            // dla każdego v takiego, że { u,v}
            //należy do P wykonaj
            foreach(var v in u.children)
            {
                if (v.colour == "black")
                Console.WriteLine(u.name + " i " + v.name + " przodek: " + Find(v).ancestor.name);
            }

        }

        private Vert Find(Vert x){
            if(x.parent != x)
                x.parent = Find(x.parent);
            return x.parent;
        }

        private void Union(Vert x, Vert y) {

            Vert xRoot = Find(x);
            Vert yRoot = Find(y);

            if (xRoot.rank > yRoot.rank)
                yRoot.parent = xRoot;

            else if (xRoot.rank < yRoot.rank)
                xRoot.parent = yRoot;

            else if (xRoot != yRoot){
                yRoot.parent = xRoot;
                xRoot.rank = xRoot.rank + 1;
            }

        }
          






    }

}
