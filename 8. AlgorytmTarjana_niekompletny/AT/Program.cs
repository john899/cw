﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmTarjana
{
    class Program
    {



        // Typy danych

        struct slistEl
        {
            unsafe slistEl* next;
            int v;
        };

        // Typ danych dla listy silnie spójnych składowych

        struct sslistEl
        {
            unsafe sslistEl* next;
            unsafe slistEl* v;
        };

        class stack
        {
            private unsafe slistEl* S;   // lista przechowująca stos

            public bool empty(void);
            public int top(void);
            public void push(int v);
            public void pop(void);

            public stack()
            {
                S = null;
            }

            // Usuwa ze stosu
            //---------------
            void pop()
            {
                if(S)
                {
                    slistEl* e = S;
                    S = S->next;
                    delete e;
                }
            }

            // Sprawdza, czy stos jest pusty
            //------------------------------
            bool empty(void)
            {
                return !S;
            }



        };


        // Zmienne globalne
        //-----------------
        unsafe int n, m, cvn,* VN,* VLow;
        unsafe bool* VS;
        stack S;
        unsafe slistEl** graf;
        unsafe sslistEl* Lscc;

 



    // Zwraca szczyt stosu
    //--------------------
    int stack::top(void)
    {
        return S->v;
    }

    // Zapisuje na stos
    //-----------------
    void stack::push(int v)
    {
        slistEl* e = new slistEl;
        e->v = v;
        e->next = S;
        S = e;
    }



    // Procedura wykonująca przejście DFS i wyznaczająca
    // silnie spójną składową.
    // v - wierzchołek startowy dla DFS
    //---------------------------------------------------
    void DFSscc(int v)
    {
        int u;
        slistEl* sccp,*p;
        sslistEl* listp;

        VN[v] = VLow[v] = ++cvn;      // Numerujemy wierzchołek i ustawiamy wstępnie parametr Low
        S.push(v);                    // Wierzchołek na stos
        VS[v] = true;                 // Zapamiętujemy, że v jest na stosie

        for (p = graf[v]; p; p = p->next) // Przeglądamy listę sąsiadów
            if (!VN[p->v])               // Jeśli sąsiad jest nieodwiedzony, to
            {
                DFSscc(p->v);             // wywołujemy dla niego rekurencyjnie DFS
                VLow[v] = min(VLow[v], VLow[p->v]); // i obliczamy parametr Low dla v
            }
            else if (VS[p->v])           // Jeśli sąsiad odwiedzony, lecz wciąż na stosie,
                VLow[v] = min(VLow[v], VN[p->v]); // to wyznaczamy parametr Low dla v

        if (VLow[v] == VN[v])          // Sprawdzamy, czy mamy kompletną składową
        {
            sccp = NULL;                // Dodajemy tę składową do listy składowych
            do
            {
                u = S.top();              // Pobieramy wierzchołek ze stosu
                S.pop();                  // Pobrany wierzchołek usuwamy ze stosu
                VS[u] = false;            // Zapamiętujemy, że nie ma go już na stosie
                p = new slistEl;          // Nowy element listy wierzchołków
                p->v = u;                 // Zapisujemy w nim wierzchołek
                p->next = sccp;           // dodajemy go na początek listy
                sccp = p;
            } while (u != v);            // Kontynuujemy aż do korzenia składowej

            listp = new sslistEl;       // Nowy element listy składowych
            listp->v = sccp;            // Zapisujemy w nim listę
            listp->next = Lscc;         // i dołączamy na początek listy składowych
            Lscc = listp;
        }
    }

    static void Main(string[] args)
    {
        int i, v, u;
        slistEl* p,*r;
        sslistEl* listp,*listr;

        cin >> n >> m;                // Odczytujemy liczbę wierzchołków i krawędzi

        graf = new slistEl*[n];     // Tworzymy tablice dynamiczne
        VN = new int[n];
        VLow = new int[n];
        VS = new bool[n];

        // Inicjujemy tablice

        for (i = 0; i < n; i++)
        {
            graf[i] = NULL;
            VN[i] = 0;
            VS[i] = false;
        }

        // Odczytujemy kolejne definicje krawędzi.

        for (i = 0; i < m; i++)
        {
            cin >> v >> u;              // Wierzchołki tworzące krawędź
            p = new slistEl;            // Tworzymy nowy element
            p->v = u;                   // Numerujemy go jako u
            p->next = graf[v];          // i dodajemy na początek listy graf[v]
            graf[v] = p;
        }

        cout << endl;

        // Wyznaczamy silnie spójne składowe

        cvn = 0;                      // Zerujemy numer wierzchołka

        Lscc = NULL;                  // Tworzymy pustą listę składowych

        for (v = 0; v < n; v++)        // Przeglądamy kolejne wierzchołki
            if (!VN[v]) DFSscc(v);       // W wierzchołku nieodwiedzonym uruchamiamy DFS

        cvn = 0;                      // cvn jest teraz licznikiem składowych
        for (listp = Lscc; listp; listp = listp->next) // Przeglądamy listę składowych
        {
            cout << "SCC" << setw(3) << ++cvn << " :"; // Wyświetlamy numer składowej
            for (p = listp->v; p; p = p->next) // Przeglądamy listę wierzchołków
                cout << setw(3) << p->v;  // Wyświetlamy wierzchołek składowej
            cout << endl;
        }

        // Usuwamy zmienne dynamiczne

        for (i = 0; i < n; i++)
        {
            p = graf[i];
            while (p)
            {
                r = p;
                p = p->next;
                delete r;
            }
        }

        listp = Lscc;
        while (listp)
        {
            p = listp->v;
            while (p)
            {
                r = p;
                p = p->next;
                delete r;
            }
            listr = listp;
            listp = listp->next;
            delete listr;
        }

        delete[] graf;
        delete[] VN;
        delete[] VLow;
        delete[] VS;

        return 0;
    }

}