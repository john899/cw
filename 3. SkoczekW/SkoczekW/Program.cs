﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkoczekW
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = 5;
            Wycieczka sk = new Wycieczka();

            List<int[,]> h_l = sk.Start(N);

  
            int[,] h = h_l.First<int[,]>();


            Console.WriteLine("\nJedno z nich: ");

            for (int i = 0; i < N; i++)
            {
                Console.WriteLine(" ");
                for (int j = 0; j < N; j++)
                    Console.Write(h[i, j] + "\t");
            }

            Console.Read();

        }
    }
}
