﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkoczekW
{
    class Wycieczka
    {

        int N = 5;
        bool q = false; //poczatkowa wartosc, musi byc false!

        int[,] h;
        int[] a = new int[8];
        int[] b = new int[8];


        List<int[,]> h_l = new List<int[,]>();


        public List<int[,]> Start(int n = 6)
        {
            this.N = n;
            h = new int[N, N];

            //tablica mozliwych ruchow
            a[0] = 2; b[0] = 1;
            a[1] = 1; b[1] = 2;
            a[2] = -1; b[2] = 2;
            a[3] = -2; b[3] = 1;
            a[4] = -2; b[4] = -1;
            a[5] = -1; b[5] = -2;
            a[6] = 1; b[6] = -2;
            a[7] = 2; b[7] = -1;

            //wypelnianei macierzy h zerami
            for (int i = 0; i < N; i++)
                for (int j = 0; j < N; j++)
                    h[i, j] = 0;

            h[0, 0] = 1;

            Probuj(2, 0, 0);

            //if (!q)
            Console.WriteLine("Znaleziono " + h_l.Count + " rozwiazan");


            return h_l;

        }


        private int IleRuchow(int x, int y)
        {
            int cntr = 0, u, v;

            for (int i = 0; i < 8; i++)
            {
                u = x + a[i];
                v = y + b[i];

                if ((0 <= u && u < N) && (0 <= v && v < N) && h[u,v] == 0)
                    cntr++;

            }

            return cntr;
        }


        void Probuj(int i, int x, int y)
        {
            int u, v, k = 0;

            //regula
            int min = 10;
            int ruch_idx = -1;

            for (int j = 0; j < 8; j++)
            {
                int u_tmp = (x + a[j]);
                int v_tmp = (y + b[j]);

                if ((0 <= u_tmp && u_tmp < N) && (0 <= v_tmp && v_tmp < N) && (h[u_tmp,v_tmp] == 0))
                {
                    if (IleRuchow(u_tmp, v_tmp) < min)
                    {
                        min = IleRuchow(u_tmp, v_tmp);
                        ruch_idx = j;
                    }
                }
            }
            //koniec reguly


            do
            {
                k++;

                u = x + a[ruch_idx];
                v = y + b[ruch_idx];       

                if ((0 <= u && u < N) && (0 <= v && v < N) && (h[u, v] == 0)) //sprawdzenie czy ruch jest mozliwy
                {
                    h[u, v] = i;    //zapisanie ruchu

                    if (i < N * N)  //czy sa jeszce pola na szachownicy
                    {
                        Probuj(i + 1, u, v); //wywolanie funkcji rekurencyjnie
                        if (!q) h[u, v] = 0;        //gdy nie ma dalszej mozliwosci ruchu
                    }
                    else
                    {
                        h_l.Add((int[,])h.Clone());
                        q = true;
                    }

                }


            } while (!q && (k <= 8));

        }


    }
}
