﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skoczek
{
    class Program
    {
        static void Main(string[] args)
        {
            const int N = 5;

            Wycieczka w = new Wycieczka();

            int[,] h = new int[N, N];


            List<int[,]> h_l = w.Start(N);

            h = h_l.First<int[,]>();

            Console.WriteLine("\nJedno z nich: ");

            for (int i = 0; i < N; i++)
            {
                Console.WriteLine(" ");
                for (int j = 0; j < N; j++)
                    Console.Write(h[i, j] + "\t");
            }

            Console.Read();

        }
    }
}