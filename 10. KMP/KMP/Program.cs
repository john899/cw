﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KMP
{
    class Program
    {

        static int[] ComputePi(int m, string P)
        {
            int[] pi = new int[m + 1];
            int k = 0;

            pi[0] = 0;

            for (int q = 1; q < m; q++){
                while ((k > 0) && (P[k] != P[q - 1]))
                    k = pi[k - 1];

                if(P[k]==P[q-1])
                    k++;

                pi[q - 1] = k;
            }

            return pi;
        }

        static List<int> FindPattern(string T, string P)
        {
            int n = T.Length;
            int m = P.Length;

            List<int> shifts = new List<int>();

            int k = 0;

            int[] pi = ComputePi(m, P);

            for (int q = 1; q <= n; q++){
                while ((k > 0) && (P[k] != T[q-1]))
                    k = pi[k];

                if (P[k] == T[q-1])
                    k++;

                if (k == m){
                    shifts.Add(q - m);
                    k = pi[k];
                }
            }

            return shifts;
        }


        static void Main(string[] args)
        {     
            string T = "bacbaababacababaababaca";  //wejscie
            string P =      "ababaca";  //wzorzec

            List<int> shifts = FindPattern(T, P); //obliczanie przesuniecia

            //wyswietlanie wyniku
            if (shifts.Count == 0)
                Console.WriteLine("Nie znaleziono wzorca");
 
            foreach (int shift in shifts)
                ShowMatch(T, P, shift); 

            Console.WriteLine();
            Console.ReadKey();

        }

        static void ShowMatch(string T, string P, int k)
        {
            Console.WriteLine(T);
            for (int i = 0; i < k; i++)
                Console.Write(" ");

            Console.Write(P + "\n");
        }

    }

}
