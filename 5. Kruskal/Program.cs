﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KruskalA
{
    class Program
    {
        static int[][] InitMatrix(int n)
        {
            int[][] graph = new int[n][];

            for (int i = 0; i < n; i++)
                graph[i] = new int[n];

            return graph;

        }

        static List<Edge> graph()
        {
            List<Edge> graph = new List<Edge>();

            graph.Add(new Edge(0, 1, 3));
            graph.Add(new Edge(0, 2, 2));
            graph.Add(new Edge(0, 3, 4));
            graph.Add(new Edge(2, 3, 3));
            graph.Add(new Edge(2, 4, 5));
            graph.Add(new Edge(3, 4, 4));


            return graph;
        }

        static void Main(string[] args)
        {
            Kruskal.Run(graph());

            Console.ReadKey();
        }
    }
}
