﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KruskalA
{
    static class Kruskal
    {

        static int[] tree;          //drzewo pomocnicze
        static List<Edge> s_tree;   //drzewo rozpinajace

        static public List<Edge> Run(List<Edge> in_graph)
        {
            int n = in_graph.Count;
            tree = InitTree(n);         //drzewo pomocnicze
            s_tree = new List<Edge>();  //drzewo rozpinajace

            List<Edge> graph = new List<Edge>(in_graph);

            graph.Sort();

            Console.WriteLine("Graf wejsciowy, posortowany: ");
            foreach (var g in graph)    //wyswietlanie grafu
                g.ShowEdge();

            for (int i = 0; i< n; i++)
                Union(graph[i]);


            Console.WriteLine("Minimalna drzewo rozpinajaca: ");
            foreach (var p in s_tree)    //wyswietlanie grafu
                p.ShowEdge();

            return s_tree;
        }


        static private int[] InitTree(int n)
        {
            int[] t = new int[n];

            for (int i = 0; i < n; i++)
                t[i] = -1;

            return t;
        }


        static private int Find(int n)
        {
            int val;

            if (tree[n] < 0)
                val =  n;
            else
                val = Find(tree[n]); //szukanie rekurencyjne

            return val;          
        }


        static private void Union(Edge e)
        {
            int n1 = e.node1;
            int n2 = e.node2;

            int root1 = Find(n1);
            int root2 = Find(n2);

            if (root1 != root2)
            {
                s_tree.Add(e);

                if (Math.Abs(tree[root1]) > Math.Abs(tree[root2])) //kompresja sciezki
                {        
                    tree[root1] += tree[root2];
                    tree[root2] = root1;
                }
                else
                {
                    tree[root2] += tree[root1];
                    tree[root1] = root2;
                }
            }
        }


    }
}
