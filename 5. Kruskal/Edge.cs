﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KruskalA
{
    public class Edge : IComparable
    {
        public int node1;
        public int node2;
        public int value;

        public Edge(int node1, int node2, int value)
        {
            this.node1 = node1;
            this.node2 = node2;
            this.value = value;
        }

        public void ShowEdge()
        {
            Console.WriteLine(this.ToString());
        }

        public override string ToString()
        {
            return (node1 + "--(" + value + ")-- " + node2);
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Edge edge = (Edge)obj;

            return this.value.CompareTo(edge.value);

        }
    }
}
