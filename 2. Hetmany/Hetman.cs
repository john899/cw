﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hetman
{
    class Hetman
    {

        //private int j;

        private int[] x = new int[8];
        private bool[] a = new bool[8];
        private bool[] b = new bool[15];
        private bool[] c = new bool[15];

        private bool q;

        public void StartHetman()
        {
            for (int i = 0; i < 15; i++)
            {
                b[i] = true;
                c[i] = true;
            }
            for (int i = 0; i < 8; i++)
            {
                a[i] = true;
                x[i] = 0;
            }

            q = false;

            Try8H(0, ref q);

            Print();

        }

        void Print()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (x[j] == i)
                        Console.Write("H\t");
                    else
                        Console.Write("_\t");
                }
                Console.WriteLine("");
            }
        }


        void Try8H(int i, ref bool q)
        {
            int j = -1;
            
            do
            {
                j++;
                q = false;

                if (a[j] && b[i + j] && c[i - j + 7])
                {
                    x[i] = j;
                    a[j] = false;
                    b[i + j] = false;
                    c[i - j + 7] = false;

                    if (i < 7)
                    {
                        Try8H(i + 1, ref q);
                        if (!q)
                        {
                            a[j] = true;
                            b[i + j] = true;
                            c[i - j + 7] = true;
                        }
                    }
                    else
                        q = true;
                }
            } while (!q && (j != 7));

        }





    }
}